package com.example.funkymunch.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// @ColumnInfo(name = "X") indica que el nombre de la columna sera X, luego se pone el valor que ira en esa columna y el tipo (val variable : y), donde y es el tipo del valor
@Entity(tableName = "UsersTable")
data class UserEntity(
    @PrimaryKey @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "name") val nombre: String,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "restaurant") val restaurante: Boolean,
    @ColumnInfo(name = "sexo") val sexo: Int
)