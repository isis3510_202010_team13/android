package com.example.funkymunch.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funkymunch.entities.PagerEntity

@Dao
interface PagerDao {
    //Obtener todos los pagers
    //Usando livedata se pueden observar los cambios de los datos. Creo que por ahora solo es necesario detectar cambios en
    // los pagers cuando se agrega uno nuevo, de manera que se actualice la vista de los pagers

    //La palabra suspend indica que este metodo se ejecutara desde una corutina
    @Query("SELECT * from PagersTable")
    fun getAllPagers(): LiveData<List<PagerEntity>>

    //Notar el ":lid" al final del query. El ":" permite obtener el parametro (lid) del metodo
    @Query("SELECT * from PagersTable WHERE pagerId = :pid")
    fun getPagerById(pid: String): PagerEntity

    //Agregar un pager a la db. La estrategia ignore descarta la peticion si ya existe un objeto igual en la db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(pager: PagerEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(pager: PagerEntity)

    @Delete
    suspend fun delete(pager: PagerEntity)
    //Metodo solo util para repoblar db
    @Query("DELETE FROM PagersTable")
    suspend fun deleteAll()

    //Ahora se crea la base de datos, que se debe actualizar al agregar mas tablas (entities)

}