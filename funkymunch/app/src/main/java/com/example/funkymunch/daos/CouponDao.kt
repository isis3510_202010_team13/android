package com.example.funkymunch.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funkymunch.entities.CouponEntity

@Dao
interface CouponDao {
    //Obtener todos los coupons
    //Usando livedata se pueden observar los cambios de los datos. Creo que por ahora solo es necesario detectar cambios en
    // los coupons cuando se agrega uno nuevo, de manera que se actualice la vista de los coupons

    //La palabra suspend indica que este metodo se ejecutara desde una corutina
    @Query("SELECT * from CouponsTable")
    fun getAllCoupons(): LiveData<List<CouponEntity>>

    //Notar el ":lid" al final del query. El ":" permite obtener el parametro (lid) del metodo
    @Query("SELECT * from CouponsTable WHERE couponId = :cid")
    fun getCouponById(cid: String): CouponEntity

    //Agregar un coupon a la db. La estrategia ignore descarta la peticion si ya existe un objeto igual en la db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(coupon: CouponEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(coupon: CouponEntity)

    @Delete
    suspend fun delete(coupon: CouponEntity)
    //Metodo solo util para repoblar db
    @Query("DELETE FROM CouponsTable")
    suspend fun deleteAll()

    //Ahora se crea la base de datos, que se debe actualizar al agregar mas tablas (entities)

}