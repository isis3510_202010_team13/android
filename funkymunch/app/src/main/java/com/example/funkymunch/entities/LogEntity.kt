package com.example.funkymunch.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// @ColumnInfo(name = "X") indica que el nombre de la columna sera X, luego se pone el valor que ira en esa columna y el tipo (val variable : y), donde y es el tipo del valor
@Entity(tableName = "LogsTable")
data class LogEntity(
    @PrimaryKey @ColumnInfo(name = "logId") val logId: String,
    @ColumnInfo(name = "type") val tipo: String,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "userId") val userId: String
){
    override fun toString(): String {
        return "LogEntity(logId=$logId, type=$tipo, date=$date, userId=$userId)"
    }
}



//Despues de crear el entity, se crea el dao