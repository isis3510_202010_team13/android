package com.example.funkymunch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth


class TableChoice : AppCompatActivity() {

    private lateinit var content: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table_choice)

        val languages = resources.getStringArray(R.array.Languages)

        val mAuth = FirebaseAuth.getInstance()
        val user = mAuth?.currentUser
        val data = user?.email

        content = data.toString()

        val grpnme = findViewById<TextView>(R.id.groupName)
        grpnme.setText(content)

        val spinner = findViewById<Spinner>(R.id.spinner)
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, languages)
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long)
                {
                    if(position != 1)
                    {
                        Toast.makeText(this@TableChoice,
                            getString(R.string.selected_item) + " " +
                                    "" + languages[position], Toast.LENGTH_SHORT).show()
                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }//android:text="This is your QR code, show it to cashier"
        }
    }

    fun sendTableRead(view: View) {
        // Do something in response to button
        //val editText = findViewById<EditText>(R.id.editText)
        // val message = editText.text.toString()
        val intent = Intent(this, TableRead::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
        finish()
    }

    
}
