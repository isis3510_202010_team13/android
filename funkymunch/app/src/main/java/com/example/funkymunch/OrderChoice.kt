package com.example.funkymunch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class OrderChoice : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_choice)
    }


    fun enterOrderINFO(view: View) {
        // Do something in response to button


        val intent = Intent(this, OrderRead::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
        finish()
    }
}
