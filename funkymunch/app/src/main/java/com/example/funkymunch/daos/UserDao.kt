package com.example.funkymunch.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funkymunch.entities.UserEntity

@Dao
interface UserDao {
    //Obtener todos los users
    //Usando livedata se pueden observar los cambios de los datos. Creo que por ahora solo es necesario detectar cambios en
    // los users cuando se agrega uno nuevo, de manera que se actualice la vista de los users

    //La palabra suspend indica que este metodo se ejecutara desde una corutina
    @Query("SELECT * from UsersTable")
    fun getAllUsers(): LiveData<List<UserEntity>>

    //Notar el ":lid" al final del query. El ":" permite obtener el parametro (lid) del metodo
    @Query("SELECT * from UsersTable WHERE userId = :uid")
    fun getUserById(uid: String): UserEntity

    //Agregar un user a la db. La estrategia ignore descarta la peticion si ya existe un objeto igual en la db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(user: UserEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(user: UserEntity)

    @Delete
    suspend fun delete(user: UserEntity)
    //Metodo solo util para repoblar db
    @Query("DELETE FROM UsersTable")
    suspend fun deleteAll()

    //Ahora se crea la base de datos, que se debe actualizar al agregar mas tablas (entities)

}