package com.example.funkymunch.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.entities.TableEntity
import com.example.funkymunch.repositories.TableRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TableViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: TableRepository

    val allTables: LiveData<List<TableEntity>>

    init {
        val tablesDao = FMDatabase.getDatabase(application, viewModelScope).tableDao()
        repository = TableRepository(tablesDao)
        allTables = repository.tables
    }


    //A traves de este metodo del viewmodel, se ejecuta la corutina no bloqueante. Por lo cual es necesario incluir los demas daos y datos en el repositorio
    //para poder hacer estas corutinas en sus respectivos viewmodels
    fun insert(table: TableEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(table)
    }

}