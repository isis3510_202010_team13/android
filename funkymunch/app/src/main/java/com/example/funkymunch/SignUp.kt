package com.example.funkymunch

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.passwordLayout


class SignUp : AppCompatActivity() {
    private var mAuth: FirebaseAuth? = null
    private val compositeDisposableOnPause = CompositeDisposable()
    private var gender = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        switchToLogIn.setOnClickListener { onBackPressed() }
        val items = listOf("Female", "Male")
        val adapter = ArrayAdapter(this, R.layout.list_item, items)
        genderDropDown.setAdapter(adapter)
        genderDropDown.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            gender = parent.getItemAtPosition(position) as String
        }
        DateInputMask(birthday).listen()
        dateOfBirthLayout.helperText = "Day/Month/Year"
        signUpButton.setOnClickListener {
            signUp()
        }
        mAuth = FirebaseAuth.getInstance()
        passwordField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (it.length < 6) {
                        passwordLayout.error = "Password should be at least length 6"
                    } else {
                        passwordLayout.error = ""
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        rePasswordField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (it.toString() != passwordField.text.toString()) {
                        rePasswordLayout.error = "Passwords don't match"
                    } else {
                        rePasswordLayout.error = ""
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        observeInternetConnection(compositeDisposableOnPause, signUpContainer)
    }

    private fun signUp() {
        if(passwordField.text.toString().isEmpty()||signUpEmailField.text.toString().isEmpty())
        {
            passwordField.setText("infracom")
            signUpEmailField.setText(" ")
            signUpEmailField.setHint("Field cannot be empty")
        }
        mAuth?.createUserWithEmailAndPassword(
            signUpEmailField.text.toString(),
            passwordField.text.toString()
        )
            ?.addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = mAuth?.currentUser
                    updateUI(user)
                } else {
                    Log.w("SignUp", "createUserWithEmail:failure", task.exception)
                    // If sign in fails, display a message to the user.
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    passwordField.setText("")
                    signUpEmailField.setText("")
                    updateUI(null)
                }
            }
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        val db = FirebaseFirestore.getInstance()
        val user = hashMapOf(
            "email" to signUpEmailField.text.toString(),
            "nacimiento" to birthday.text.toString(),
            "nombre" to signUpNameField.text.toString(),
            "sexo" to gender
        )
        db.collection("User")
            .add(user)
            .addOnSuccessListener { documentReference ->
            }
            .addOnFailureListener { e ->
            }
        currentUser?.let {
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
    override fun onPause() {
        compositeDisposableOnPause.clear()
        super.onPause()
    }
}

class DateInputMask(val input: EditText) {

    fun listen() {
        input.addTextChangedListener(mDateEntryWatcher)
    }

    private val mDateEntryWatcher = object : TextWatcher {

        var edited = false
        val dividerCharacter = "/"

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (edited) {
                edited = false
                return
            }

            var working = getEditText()

            working = manageDateDivider(working, 2, start, before)
            working = manageDateDivider(working, 5, start, before)

            edited = true
            input.setText(working)
            input.setSelection(input.text.length)
        }

        private fun manageDateDivider(
            working: String,
            position: Int,
            start: Int,
            before: Int
        ): String {
            if (working.length == position) {
                return if (before <= position && start < position)
                    working + dividerCharacter
                else
                    working.dropLast(1)
            }
            return working
        }

        private fun getEditText(): String {
            return if (input.text.length >= 10)
                input.text.toString().substring(0, 10)
            else
                input.text.toString()
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    }

}