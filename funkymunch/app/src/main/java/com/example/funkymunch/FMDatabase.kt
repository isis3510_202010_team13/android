package com.example.funkymunch

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.funkymunch.daos.*
import com.example.funkymunch.entities.*
import com.example.funkymunch.repositories.LogRepository
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.firestoreSettings
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

//Debo confirmar comportamiento, pero creo que para las otras entities solo es necesario poner una coma y repetir el entity::class dentro del array
@Database(entities = [LogEntity::class,CouponEntity::class,PagerEntity::class,TableEntity::class,UserEntity::class], version = 1, exportSchema = false)
abstract class FMDatabase : RoomDatabase() {

    abstract fun logDao(): LogDao
    abstract fun couponDao(): CouponDao
    abstract fun pagerDao(): PagerDao
    abstract fun tableDao(): TableDao
    abstract fun userDao(): UserDao



    companion object {

        //Se usa un singleton de la db para evitar que se creen y abran varias a la vez
        @Volatile
        private var INSTANCE: FMDatabase? = null

        @Volatile
        private var firestore = Firebase.firestore


        fun getFireStore(): FirebaseFirestore {
            return firestore
        }

        private val settings = firestoreSettings {
            isPersistenceEnabled = true
        }

        //Este metodo creara la db la primera vez que se ejecute
        fun getDatabase(context: Context, scope: CoroutineScope): FMDatabase {
            firestore.firestoreSettings = settings
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            //FMDatabase es el nombre de la db, pero tambien de esta clase. Se pueden cambiar de ser necesario
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FMDatabase::class.java,
                    "FMDatabase"
                ).addCallback(FMDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

    //Clase que permite hacer override para poblar la db en onOpen()
    private class FMDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch { populateDatabase(database.logDao()) }

            }
        }

        //Metodo de prueba que puebla la db con datos de prueba
        @RequiresApi(Build.VERSION_CODES.O)
        suspend fun populateDatabase(logDao: LogDao) {

            //Esto es solo para borrar los logs de prueba al ejecutar de nuevo
            logDao.deleteAll()
            logDao.deleteLogByIdFS("id1", firestore)
            logDao.deleteLogByIdFS("id2", firestore)

            val currentDateTime = LocalDateTime.now()
            val str = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))



            var log = LogEntity("id1", "pager", str, "uid")
            logDao.insert(log)
            logDao.insertFS(log, firestore)
            log = LogEntity("id2", "table", str, "uid")
            logDao.insert(log)
            logDao.insertFS(log, firestore)
        }
    }

}

//Ahora creamos el repositorio, que maneja los metodos de los daos