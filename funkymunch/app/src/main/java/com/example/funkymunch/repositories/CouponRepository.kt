package com.example.funkymunch.repositories

import androidx.lifecycle.LiveData
import com.example.funkymunch.daos.CouponDao
import com.example.funkymunch.entities.CouponEntity


// A Repository manages queries and allows you to use multiple backends. In the most common example,
// the Repository implements the couponic for deciding whether to fetch data from a network or use results cached in a local database.
class CouponRepository(private val couponDao: CouponDao) {

    val coupons: LiveData<List<CouponEntity>> = couponDao.getAllCoupons()

    suspend fun insert(coupon: CouponEntity) {
        couponDao.insert(coupon)
    }

}

//Despues del repositorio, se crea el viewmodel. De ser necesario, este repositorio se puede modificar para que se llame DataRepository
//e incluya los otros datos y daos como de las mesas, pagers, usuario y otras clases.
