package com.example.funkymunch

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.app.NotificationCompat
import kotlin.random.Random

class Details : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val previous = intent.getStringExtra(EXTRA_MESSAGE)
        if(previous.equals("order"))
        {
            val toptxt = findViewById<TextView>(R.id.Toptext)
            val order = Random.nextInt(3, 30)
            val orderText = "Your order number is: "
            toptxt.setText(orderText)
            val numtxt = findViewById<TextView>(R.id.numT)
            numtxt.setText(order.toString())
            val timetxt = findViewById<TextView>(R.id.tempo)
            var tempo = 0
            if(order < 10){
                tempo = Random.nextInt(3, 10)
            }
            else{
                tempo = Random.nextInt(15, 25)
            }
            timetxt.setText("Estimated time for your order " +tempo.toString() + " min")
        }
        else
        {
            val toptxt = findViewById<TextView>(R.id.Toptext)
            val place = Random.nextInt(15, 60)
            val placeText = "Your place in line is: "
            toptxt.setText(placeText)
            val numtxt = findViewById<TextView>(R.id.numT)
            numtxt.setText(place.toString())
            val timetxt = findViewById<TextView>(R.id.tempo)
            var tempo = 0
            if(place < 10){
                tempo = Random.nextInt(10, 30)
            }
            else{
                tempo = Random.nextInt(30, 70)
            }
            timetxt.setText("Estimated time to be seated " + tempo.toString() + " min")
        }


        //utilice set text para cambiar el num y el tiempo estimado

    }

    private var mNotifyManager: NotificationManager? = null
    fun createNotificationChannel() {
        mNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create a NotificationChannel
            val notificationChannel = NotificationChannel("primary_notification_channel", "Mascot Notification", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true);
            notificationChannel.lightColor = Color.RED;
            notificationChannel.enableVibration(true);
            notificationChannel.description = "Notification from Mascot";
            mNotifyManager?.createNotificationChannel(notificationChannel);
        }
    }

    private fun getNotificationBuilder(): NotificationCompat.Builder? {
        return NotificationCompat.Builder(this, "primary_notification_channel")
            .setContentTitle("Your table is ready")
            .setContentText("You can come back to the restaurant!")
            .setSmallIcon(R.drawable.ic_android)
    }


    fun simulateComplete(view: View)
    {

        val intent = Intent(this, MainMenuActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
        finish()

    }
}
