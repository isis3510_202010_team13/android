package com.example.funkymunch.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// @ColumnInfo(name = "X") indica que el nombre de la columna sera X, luego se pone el valor que ira en esa columna y el tipo (val variable : y), donde y es el tipo del valor
@Entity(tableName = "CouponsTable")
data class CouponEntity(
    @PrimaryKey @ColumnInfo(name = "couponId") val CouponId: String,
    @ColumnInfo(name = "descrip") val descrip: String,
    @ColumnInfo(name = "discount") val discount: Int,
    @ColumnInfo(name = "restId") val restId: String,
    @ColumnInfo(name = "timesU") val timesU: Int
)

//Despues de crear el entity, se crea el dao