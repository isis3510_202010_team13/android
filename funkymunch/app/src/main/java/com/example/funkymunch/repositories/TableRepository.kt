package com.example.funkymunch.repositories

import androidx.lifecycle.LiveData
import com.example.funkymunch.daos.TableDao
import com.example.funkymunch.entities.TableEntity


// A Repository manages queries and allows you to use multiple backends. In the most common example,
// the Repository implements the tableic for deciding whether to fetch data from a network or use results cached in a local database.
class TableRepository(private val tableDao: TableDao) {

    val tables: LiveData<List<TableEntity>> = tableDao.getAllTables()

    suspend fun insert(table: TableEntity) {
        tableDao.insert(table)
    }

}

//Despues del repositorio, se crea el viewmodel. De ser necesario, este repositorio se puede modificar para que se llame DataRepository
//e incluya los otros datos y daos como de las mesas, pagers, usuario y otras clases.
