package com.example.funkymunch

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NotificationCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.funkymunch.entities.UserEntity
import com.example.funkymunch.viewModels.UserViewModel
import com.google.android.gms.common.internal.FallbackServiceBroker
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main_menu.*

const val EXTRA_MESSAGE = "com.example.funkymunch.MESSAGE"

class MainMenuActivity : AppCompatActivity() {
    companion object {
        private const val PRIMARY_CHANNEL_ID = "primary_notification_channel"
        private const val NOTIFICATION_ID = 0
    }


    private lateinit var userViewModel: UserViewModel
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var mAuth: FirebaseAuth? = null
    private val compositeDisposableOnPause = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        val usuario = mAuth?.currentUser
        var uid = usuario?.uid.toString()
        var name = usuario?.displayName.toString()
        var date = "01/02/2003"
        var email = usuario?.email.toString()
        var res = false
        var sexo = 0
        var ue = UserEntity(uid, name, date, email, res, sexo)
        userViewModel.insert(ue)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            /**view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
             */
            val intent = Intent(this@MainMenuActivity, LogsActivity::class.java)
            startActivity(intent)

        }




        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.menu.findItem(R.id.nav_signOut).setOnMenuItemClickListener {
            logout()
            true
        }
        navigationView.menu.findItem(R.id.pushNotification).setOnMenuItemClickListener {
            pushNotification()
            true
        }

        mAuth = FirebaseAuth.getInstance()
        val user = mAuth?.currentUser

        val headerView = navigationView.getHeaderView(0)
        val navUsername = headerView.findViewById(R.id.drawerDisplayName) as TextView
        navUsername.text = user?.displayName
        val navEmail = headerView.findViewById(R.id.drawerEmail) as TextView
        navEmail.text = user?.email
        createNotificationChannel()
        observeInternetConnection(compositeDisposableOnPause,nav_view)


    }


    private var mNotifyManager: NotificationManager? = null
    fun createNotificationChannel() {
        mNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create a NotificationChannel
            val notificationChannel = NotificationChannel(PRIMARY_CHANNEL_ID, "Mascot Notification", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true);
            notificationChannel.lightColor = Color.RED;
            notificationChannel.enableVibration(true);
            notificationChannel.description = "Notification from Mascot";
            mNotifyManager?.createNotificationChannel(notificationChannel);
        }
    }

    private fun getNotificationBuilder(): NotificationCompat.Builder? {
        return NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
            .setContentTitle("Your table is ready")
            .setContentText("You can come back to the restaurant!")
            .setSmallIcon(R.drawable.ic_android)
    }

    private fun pushNotification() {
        val notifyBuilder = getNotificationBuilder()
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        notifyBuilder?.setSound(alarmSound)
        mNotifyManager!!.notify(NOTIFICATION_ID, notifyBuilder!!.build())
    }

    private fun logout() {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.nav_signOut -> true.also {

            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onPause() {
        compositeDisposableOnPause.clear()
        super.onPause()
    }

    //val tblBtn: Button = findViewById(R.id.buttonTable)
    fun sendTableInfo(view: View) {
        // Do something in response to button
        //val editText = findViewById<EditText>(R.id.editText)
        // val message = editText.text.toString()
        val intent = Intent(this, TableChoice::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
    }
    fun sendOrderInfo(view: View) {
        // Do something in response to button
        //val editText = findViewById<EditText>(R.id.editText)
        // val message = editText.text.toString()
        val intent = Intent(this, OrderRead::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
    }

}
