package com.example.funkymunch

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter

class OrderRead : AppCompatActivity(), OutcomingNfcManager.NfcActivity {

    private var nfcAdapter: NfcAdapter? = null

    private val isNfcSupported: Boolean =
        this.nfcAdapter != null

    private lateinit var outcomingNfcCallback: OutcomingNfcManager
    private lateinit var content: String
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_read)

        val imageview = findViewById<ImageView>(R.id.QRImage)
        mAuth = FirebaseAuth.getInstance()
        val user = mAuth?.currentUser
        val data = user?.email + ", " + user?.displayName
        content = data

        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        imageview.setImageBitmap(bitmap)


    }


    fun nfcButton(view: View)
    {
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter==null) {
            Toast.makeText(this, "NFC is not supported on this device", Toast.LENGTH_SHORT).show()
            finish()
        }
        else if (!nfcAdapter?.isEnabled!!) {
            Toast.makeText(
                this,
                "NFC disabled on this device. Turn on to proceed",
                Toast.LENGTH_SHORT
            ).show()
        }
        else
        {
            this.outcomingNfcCallback = OutcomingNfcManager(this)
            this.nfcAdapter?.setOnNdefPushCompleteCallback(outcomingNfcCallback, this)
            this.nfcAdapter?.setNdefPushMessageCallback(outcomingNfcCallback, this)
            val intent = Intent(this, Details::class.java).apply {
                putExtra(EXTRA_MESSAGE, "order")
            }


            startActivity(intent)
            finish()
        }

    }

    fun simulateRead(view: View) {
        // Do something in response to button


        val intent = Intent(this, Details::class.java).apply {
            putExtra(EXTRA_MESSAGE, "order")
        }
        startActivity(intent)
        finish()
    }

    override fun getOutcomingMessage(): String =
        content

    override fun signalResult() {
        runOnUiThread {
            Toast.makeText(this, R.string.message_beaming_complete, Toast.LENGTH_SHORT).show()
        }
    }
}
