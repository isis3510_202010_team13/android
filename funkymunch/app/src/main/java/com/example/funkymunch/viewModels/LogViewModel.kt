package com.example.funkymunch.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.entities.LogEntity
import com.example.funkymunch.repositories.LogRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LogViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: LogRepository

    val allLogs: LiveData<List<LogEntity>>

    init {
        val logsDao = FMDatabase.getDatabase(application, viewModelScope).logDao()
        repository = LogRepository(logsDao)
        allLogs = repository.logs
    }


    //A traves de este metodo del viewmodel, se ejecuta la corutina no bloqueante. Por lo cual es necesario incluir los demas daos y datos en el repositorio
    //para poder hacer estas corutinas en sus respectivos viewmodels
    fun insert(log: LogEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(log)
    }

    fun insertFS(log: LogEntity){
        repository.insertFS(log)
    }

}