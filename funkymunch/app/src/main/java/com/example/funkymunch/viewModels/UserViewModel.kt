package com.example.funkymunch.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.entities.UserEntity
import com.example.funkymunch.repositories.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: UserRepository

    val allUsers: LiveData<List<UserEntity>>

    init {
        val usersDao = FMDatabase.getDatabase(application, viewModelScope).userDao()
        repository = UserRepository(usersDao)
        allUsers = repository.users
    }


    //A traves de este metodo del viewmodel, se ejecuta la corutina no bloqueante. Por lo cual es necesario incluir los demas daos y datos en el repositorio
    //para poder hacer estas corutinas en sus respectivos viewmodels
    fun insert(user: UserEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(user)
    }

}