package com.example.funkymunch.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.entities.CouponEntity
import com.example.funkymunch.repositories.CouponRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CouponViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: CouponRepository

    val allCoupons: LiveData<List<CouponEntity>>

    init {
        val couponsDao = FMDatabase.getDatabase(application, viewModelScope).couponDao()
        repository = CouponRepository(couponsDao)
        allCoupons = repository.coupons
    }


    //A traves de este metodo del viewmodel, se ejecuta la corutina no bloqueante. Por lo cual es necesario incluir los demas daos y datos en el repositorio
    //para poder hacer estas corutinas en sus respectivos viewmodels
    fun insert(coupon: CouponEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(coupon)
    }

}