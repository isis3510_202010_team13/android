package com.example.funkymunch.repositories

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.daos.LogDao
import com.example.funkymunch.entities.LogEntity


// A Repository manages queries and allows you to use multiple backends. In the most common example,
// the Repository implements the logic for deciding whether to fetch data from a network or use results cached in a local database.
class LogRepository(private val logDao: LogDao) {

    val logs: LiveData<List<LogEntity>> = logDao.getAllLogs()
    val fsDB = FMDatabase.getFireStore()

    suspend fun insert(log: LogEntity) {
        logDao.insert(log)
    }

    fun insertFS(log: LogEntity){
        logDao.insertFS(log,fsDB)
    }


}

//Despues del repositorio, se crea el viewmodel. De ser necesario, este repositorio se puede modificar para que se llame DataRepository
//e incluya los otros datos y daos como de las mesas, pagers, usuario y otras clases.
