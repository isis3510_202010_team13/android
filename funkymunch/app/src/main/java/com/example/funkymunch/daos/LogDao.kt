package com.example.funkymunch.daos

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funkymunch.entities.LogEntity
import com.example.funkymunch.FMDatabase
import com.google.firebase.firestore.FirebaseFirestore

@Dao
interface LogDao {
    //Obtener todos los logs
    //Usando livedata se pueden observar los cambios de los datos. Creo que por ahora solo es necesario detectar cambios en
    // los logs cuando se agrega uno nuevo, de manera que se actualice la vista de los logs

    //La palabra suspend indica que este metodo se ejecutara desde una corutina
    @Query("SELECT * from LogsTable")
    fun getAllLogs(): LiveData<List<LogEntity>>

    //Notar el ":lid" al final del query. El ":" permite obtener el parametro (lid) del metodo
    @Query("SELECT * from LogsTable WHERE logId = :lid")
    fun getLogById(lid: String): LogEntity

    //Agregar un log a la db. La estrategia ignore descarta la peticion si ya existe un objeto igual en la db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(log: LogEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(log: LogEntity)

    @Delete
    suspend fun delete(log: LogEntity)
    //Metodo solo util para repoblar db
    @Query("DELETE FROM LogsTable")
    suspend fun deleteAll()

    fun insertFS(log: LogEntity, db: FirebaseFirestore){
        db.collection("Logs")
            .add(logHashMap(log))
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    fun logHashMap(log: LogEntity): HashMap<String, String> {
        return hashMapOf(
            "date" to "${log.date}",
            "restId" to "${log.logId}",
            "tipo" to "${log.tipo}",
            "userId" to "${log.userId}"
        )
    }

    fun deleteLogByIdFS(id: String, db:FirebaseFirestore){
        db.collection("Logs").document(id).delete()
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted") }
            .addOnFailureListener { e -> Log.w(TAG, "Error deleting document",e) }
    }

    //Ahora se crea la base de datos, que se debe actualizar al agregar mas tablas (entities)


}