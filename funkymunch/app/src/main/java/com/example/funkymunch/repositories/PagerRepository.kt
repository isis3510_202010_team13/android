package com.example.funkymunch.repositories

import androidx.lifecycle.LiveData
import com.example.funkymunch.daos.PagerDao
import com.example.funkymunch.entities.PagerEntity


// A Repository manages queries and allows you to use multiple backends. In the most common example,
// the Repository implements the pageric for deciding whether to fetch data from a network or use results cached in a local database.
class PagerRepository(private val pagerDao: PagerDao) {

    val pagers: LiveData<List<PagerEntity>> = pagerDao.getAllPagers()

    suspend fun insert(pager: PagerEntity) {
        pagerDao.insert(pager)
    }

}

//Despues del repositorio, se crea el viewmodel. De ser necesario, este repositorio se puede modificar para que se llame DataRepository
//e incluya los otros datos y daos como de las mesas, pagers, usuario y otras clases.
