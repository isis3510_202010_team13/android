package com.example.funkymunch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.funkymunch.viewModels.LogViewModel

class LogsActivity : AppCompatActivity() {

    private lateinit var logViewModel: LogViewModel

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logs)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = LogListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        logViewModel = ViewModelProvider(this).get(LogViewModel::class.java)
        logViewModel.allLogs.observe(this, Observer{
            logs -> logs?.let { adapter.setLogs(it) }
        })
    }




}