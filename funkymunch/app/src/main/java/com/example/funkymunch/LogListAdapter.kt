package com.example.funkymunch

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.funkymunch.entities.LogEntity

class LogListAdapter internal constructor(context: Context) : RecyclerView.Adapter<LogListAdapter.LogViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var logs = emptyList<LogEntity>()

    inner class LogViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val logItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogViewHolder{
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return LogViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: LogViewHolder, position: Int) {
        val current = logs[position]
        val info = "${current.logId}\n ${current.date} ${current.tipo}"
        holder.logItemView.text = info

    }

    internal fun setLogs(logs: List<LogEntity>) {
        this.logs = logs
        notifyDataSetChanged()
    }

    override fun getItemCount() = logs.size
}