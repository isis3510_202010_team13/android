package com.example.funkymunch.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funkymunch.entities.TableEntity

@Dao
interface TableDao {
    //Obtener todos los tables
    //Usando livedata se pueden observar los cambios de los datos. Creo que por ahora solo es necesario detectar cambios en
    // los tables cuando se agrega uno nuevo, de manera que se actualice la vista de los tables

    //La palabra suspend indica que este metodo se ejecutara desde una corutina
    @Query("SELECT * from TablesTable")
    fun getAllTables(): LiveData<List<TableEntity>>

    //Notar el ":lid" al final del query. El ":" permite obtener el parametro (lid) del metodo
    @Query("SELECT * from TablesTable WHERE tableId = :tid")
    fun getTableById(tid: String): TableEntity

    //Agregar un table a la db. La estrategia ignore descarta la peticion si ya existe un objeto igual en la db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(table: TableEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(table: TableEntity)

    @Delete
    suspend fun delete(table: TableEntity)
    //Metodo solo util para repoblar db
    @Query("DELETE FROM TablesTable")
    suspend fun deleteAll()

    //Ahora se crea la base de datos, que se debe actualizar al agregar mas tablas (entities)

}