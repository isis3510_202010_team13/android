package com.example.funkymunch.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// @ColumnInfo(name = "X") indica que el nombre de la columna sera X, luego se pone el valor que ira en esa columna y el tipo (val variable : y), donde y es el tipo del valor
@Entity(tableName = "PagersTable")
data class PagerEntity(
    @PrimaryKey @ColumnInfo(name = "pagerId") val pagerId: String,
    @ColumnInfo(name = "turn") val turn: Int,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "restId") val restId: String,
    @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "ready") val ready: Boolean
)

//Despues de crear el entity, se crea el dao