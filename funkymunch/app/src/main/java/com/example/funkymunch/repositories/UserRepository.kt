package com.example.funkymunch.repositories

import androidx.lifecycle.LiveData
import com.example.funkymunch.daos.UserDao
import com.example.funkymunch.entities.UserEntity


// A Repository manages queries and allows you to use multiple backends. In the most common example,
// the Repository implements the useric for deciding whether to fetch data from a network or use results cached in a local database.
class UserRepository(private val userDao: UserDao) {

    val users: LiveData<List<UserEntity>> = userDao.getAllUsers()

    suspend fun insert(user: UserEntity) {
        userDao.insert(user)
    }

}

//Despues del repositorio, se crea el viewmodel. De ser necesario, este repositorio se puede modificar para que se llame DataRepository
//e incluya los otros datos y daos como de las mesas, pagers, usuario y otras clases.
