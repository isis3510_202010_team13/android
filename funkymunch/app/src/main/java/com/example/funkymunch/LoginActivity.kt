package com.example.funkymunch

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private val compositeDisposableOnPause = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        switchToSignUp.setOnClickListener {
            val intent = Intent(this, SignUp::class.java)
            startActivity(intent)
        }
        mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth?.currentUser
        updateUI(currentUser)
        signInButton.setOnClickListener { signIn() }

        observeInternetConnection(compositeDisposableOnPause, loginContainer)
    }


    private fun updateUI(currentUser: FirebaseUser?) {
        currentUser?.let {
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun signIn() {
        if(loginPasswordField.text.toString().isEmpty()||loginEmailField.text.toString().isEmpty())
        {
            loginPasswordField.setText("infracom")
            loginEmailField.setText(" ")
        }
        mAuth?.signInWithEmailAndPassword(
            loginEmailField.text.toString(),
            loginPasswordField.text.toString()
        )
            ?.addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = mAuth?.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    Log.d("LoginActivity", "failed", task.exception)
                    loginPasswordField.setText("")
                    loginEmailField.setText("")
                    updateUI(null)
                }
            }
    }

    override fun onPause() {
        compositeDisposableOnPause.clear()
        super.onPause()
    }
}

fun Activity.observeInternetConnection(compositeDisposable: CompositeDisposable, loginContainer: View) {
    val snackbar = Snackbar.make(
        loginContainer,
        "Please check yout internet connection",
        Snackbar.LENGTH_INDEFINITE
    )
    val disposable = ReactiveNetwork.observeNetworkConnectivity(this)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { connectivity ->
            if (!connectivity.available()) {
                snackbar.show()
            } else {
                snackbar.dismiss()
            }
        }
    compositeDisposable.add(disposable)
}
