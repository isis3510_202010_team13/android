package com.example.funkymunch.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funkymunch.FMDatabase
import com.example.funkymunch.entities.PagerEntity
import com.example.funkymunch.repositories.PagerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PagerViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: PagerRepository

    val allPagers: LiveData<List<PagerEntity>>

    init {
        val pagersDao = FMDatabase.getDatabase(application, viewModelScope).pagerDao()
        repository = PagerRepository(pagersDao)
        allPagers = repository.pagers
    }


    //A traves de este metodo del viewmodel, se ejecuta la corutina no bloqueante. Por lo cual es necesario incluir los demas daos y datos en el repositorio
    //para poder hacer estas corutinas en sus respectivos viewmodels
    fun insert(pager: PagerEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(pager)
    }

}