package com.example.restaurante

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity


const val EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE"
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setSupportActionBar(findViewById(R.id.my_toolbar))
        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val activeOrd = sp.getInt("ActiveOrd", 0)
        val loggedon = sp.getInt("LoggedOn", 1)
        if(activeOrd==1)//pager
        {
            val intent = Intent(this, Detalles2::class.java).apply {
                putExtra(EXTRA_MESSAGE, "")
            }
            startActivity(intent)
        }
        else if(activeOrd==2)//table
        {
            val intent = Intent(this, Detalles::class.java).apply {
                putExtra(EXTRA_MESSAGE, "")
            }
            startActivity(intent)
        }
        else if(loggedon==1)//not logged in
        {
            val intent = Intent(this, LogOrCreate::class.java).apply {
                putExtra(EXTRA_MESSAGE, "")
            }
            startActivity(intent)
        }

    }
    /** Called when the user taps the Send button */
    fun sendMessage(view: View) {
        // Do something in response to button
        //val editText = findViewById<EditText>(R.id.editText)
       // val message = editText.text.toString()
        val intent = Intent(this, pedidoActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
    }
    fun sendMessage2(view: View) {
        // Do something in response to button
        //val editText = findViewById<EditText>(R.id.editText)
        // val message = editText.text.toString()
        val intent = Intent(this, turnoActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
    }
    fun logOutButton(view: View)
    {
        val intent = Intent(this, LogOrCreate::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")

            val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
            val editor = sp.edit()
            editor.putInt("LoggedOn", 1)
            editor.commit()

        }
        startActivity(intent)

    }
    fun logsButton(view: View)
    {
        this.getSharedPreferences("your_prefs", Activity.MODE_PRIVATE).edit().clear().commit();
    }
}
