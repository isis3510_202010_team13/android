package com.example.restaurante

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity


class LogOrCreate : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_or_create)

    }

    fun create(view: View) {
        val intent = Intent(this, loginActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")

        }
        startActivity(intent)
        finish()
    }

    fun login(view: View) {
        val intent = Intent(this, LoginUser::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")

        }
        startActivity(intent)
        finish()
    }
}