package com.example.restaurante

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random


class Detalles2 : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles2)

        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val activeOrd = sp.getInt("ActiveOrd", 0)
        if(activeOrd==1)
        {
            val GrpName = sp.getString("GrpName","")
            val textView = findViewById<TextView>(R.id.textView3).apply {text = GrpName}
        }
        else if(activeOrd==0)
        {
            val editor = sp.edit()
            editor.putInt("ActiveOrd", 1)
            editor.commit()

            val message = intent.getStringExtra(EXTRA_MESSAGE)
            val textView = findViewById<TextView>(R.id.textView3).apply {text = message}
        }

        val randomValues = List(10) { Random.nextInt(0, 10) }
        val textView = findViewById<TextView>(R.id.textView)
        textView.text = "You're number "+randomValues[0].toString()+" in line"

    }

    fun sendMessage(view: View) {
        // Do something in response to button
        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putInt("ActiveOrd", 0)
        editor.commit()
        finish()

    }

    fun test(view: View){
        Log.e("Click","Yes")
    }

}
