package com.example.restaurante

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.activity_pedido.*

class pedidoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pedido)

        val imageview = findViewById<ImageView>(R.id.imageView)

        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val nameLog = sp.getString("name", "")
        val emailLog = sp.getString("email", "")

        val content = nameLog

        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        imageview.setImageBitmap(bitmap)

    }

    fun sendMessage(view: View) {
        // Do something in response to button


        val intent = Intent(this, Detalles::class.java).apply {
            putExtra(EXTRA_MESSAGE, "")
        }
        startActivity(intent)
        finish()
    }
}
