package com.example.restaurante

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity

class loginActivity: Activity(), AdapterView.OnItemSelectedListener{

    //val languages = resources.getStringArray(R.array.genders)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val spinner: Spinner = findViewById(R.id.genderSpinner)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.genders,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

    }
    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }
    fun login(view: View)
    {
        val name = findViewById<EditText>(R.id.editText3)
        val nameMessage = name.text.toString()
        val email = findViewById<EditText>(R.id.editText2)
        val emailMessage = email.text.toString()

        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putInt("LoggedOn", 0)
        editor.putString("name", nameMessage)
        editor.putString("email", emailMessage)

        editor.commit()
        finish()

    }

}