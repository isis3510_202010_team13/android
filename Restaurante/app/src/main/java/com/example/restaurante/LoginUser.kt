package com.example.restaurante

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

class LoginUser : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_user)

    }

    fun login(view: View)
    {
        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val editor = sp.edit()

        val name = findViewById<EditText>(R.id.NameField)
        val nameMessage = name.text.toString()
        val email = findViewById<EditText>(R.id.emailField)
        val emailMessage = email.text.toString()



        editor.putInt("LoggedOn", 0)
        editor.putString("name", nameMessage)
        editor.putString("email", emailMessage)

        editor.commit()
        finish()

    }
}
