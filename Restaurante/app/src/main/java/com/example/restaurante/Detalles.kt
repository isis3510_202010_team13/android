package com.example.restaurante

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.random.Random

class Detalles : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)

        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val activeOrd = sp.getInt("ActiveOrd", 0)

        if(activeOrd==1)
        {
            val GrpName = sp.getString("GrpName","")
            val textView = findViewById<TextView>(R.id.textView3).apply {text = GrpName}
        }
        else if(activeOrd==0) {
            val editor = sp.edit()
            editor.putInt("ActiveOrd", 2)
            editor.commit()
        }

        val randomValues = List(10) { Random.nextInt(0, 50) }
        val textView = findViewById<TextView>(R.id.editText4)
        textView.text = randomValues[0].toString()

    }
    fun resetOrder(view: View) {
        // Do something in response to button
        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putInt("ActiveOrd", 0)
        editor.commit()
        finish()
    }
}
