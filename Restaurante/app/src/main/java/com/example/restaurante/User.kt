package com.example.restaurante

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "user_table")
class User(@PrimaryKey(autoGenerate = true) val id: Int, @ColumnInfo(name = "user") val user: String){

}