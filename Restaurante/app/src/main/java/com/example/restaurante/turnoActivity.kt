package com.example.restaurante

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity


class turnoActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turno)


    }

    fun sendMessage(view: View) {
        // Do something in response to button
       // val editTextValue = editText.text
        val editText = findViewById<EditText>(R.id.editNom)
        val message = editText.text.toString()

        val editTextNum = findViewById<EditText>(R.id.editNum)
        val numMessage = editTextNum.text.toString()

        val sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putString("GrpName", message)
        editor.putString("GrpNum", numMessage)
        editor.commit()

        val intent = Intent(this, Detalles2::class.java).apply {
            putExtra(EXTRA_MESSAGE, message)
        }
        startActivity(intent)
        finish()
    }
}
